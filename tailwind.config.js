const colors = require('tailwindcss/colors')
/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./**/*.{html,js}"],
  theme: {
    extend: {
      fontFamily: {
        'dmsans': ['DM Sans',' sans-serif'],
        'pilow': ['Pilowlava', 'mono'],
      },

      letterSpacing: {
        big: '0.7em',
      }
    },
    colors: {
      beige: '#F0ECE4',
      black: '#000000',
      white: '#ffffff',
      gray: colors.gray,
      stone: colors.stone,
      neutral: colors.neutral,
      green: '#8A8357',
    },
    }
  }

